# PÁGINAS WEB DE LOS CAPÍTULOS DEL MÓDULO IUN

## UD-1
### ------------------------------------------ CONCEPTOS BÁSICOS
___
Los `objetivos` es:
1. [X] Distinguir los agentes que intervienen en un sistema de comunicaciones.
2. [X] Conocer y diferenciar las magnitudes fundamentales de una señal.

## UD-2
### ------------------------------------------ SISTEMAS DE RADIOCOMUNICACIONES
___
Los `objetivos` es:
1. [X] Identificar los distintos sistemas de radiocomunicaciones y sus elementos.
2. [X] Describir la estructura de las redes fijas y móviles de radiocomunicaciones.
3. [X] Clasificar los sistemas de radiocomunicaciones según su tecnología.

## UD-3
### ------------------------------------------ ELEMENTOS BÁSICOS

___
Los `objetivos` son:
1. [X] Describir las características de los equipos y medios de transmisión.
2. [X] Analizar los bloques funcionales de los equipos de transmisión y recepción.
3. [X] Conocer las principales características de los sistemas radiantes.
4. [X] Describir la instalación eléctrica de una instalación de radiocomunicaciones.
5. [X] Valorar la importancia del uso de sistemas de alimentación ininterrumpida.
6. [ ] Conocer las normas reguladoras del uso del espectro radioeléctrico.

## UD-4
### ------------------------------------------

___
Los `objetivos` son:
1. [X] Interpretar documentación técnica, planos y esquemas.
2. [ ] Ubicar los elementos de radiocomunicaciones.

## UD-5
### ------------------------------------------ EL SOFTWARE EN LOS EQUIPOS
___
Los `objetivos` es:
1. [ ] Interpretar documentación técnica, planos y esquemas.
2. [X] Ubicar los elementos de radiocomunicaciones.

## UD-6
### ------------------------------------------

___
Los `objetivos` son:
1. [ ] Replantear instalaciones de radiocomunicaciones.
2. [X] Identificar y marcar la posición de los equipos.
3. [X] Reconocer e interconectar los elementos auxiliares de un sistema de radiocomunicaciones.
4. [ ] Instalar y orientar los sistemas de antenas.
5. [X] Etiquetar los equipos y líneas de transmisión.

## UD-7
### ------------------------------------------

___
Los `objetivos` son:
1. [X] Conocer los intrumentos de medida utilizados en las instalaciones de radiocomunicaciones.
2. [X] Realizar medidas de ROE.
3. [ ] Conocer y realizar las pruebas de aceptación de los equipos.

## UD-8
### ------------------------------------------

___
Los `objetivos` son:
1. [ ] Instalar equipos y elementos auxiliares de redes fijas y móviles, interpretando documentación técnica y aplicando técnicas de conexión y montaje.
2. [ ] Poner en servicio equipos de radiocomunicaciones interpretando y ejecutando planes de prueba.
3. [ ] Cumplir las normas de prevención de riesgos laborales y de protección ambiental, identificando los riesgos asociados, las medidas y equipos para prevenirlos en instalaciones de radiocomunicaciones.

## UD-9
### ------------------------------------------

___
Los `objetivos` son:
1. [X] Reconocer la importancia del mantenimiento dentro de la labor del instalador de telecomunicaciones.
2. [X] Reparar averías empleando las técnicas básicas de diagnóstico y localización de averías.
3. [X] Redactar y comprender la documentación básica en los procesos de mantenimiento.
4. [ ] Mantener equipos de radiocomunicaciones aplicando mantenimiento preventivo.

## UD-10
### ------------------------------------------

___
Los `objetivos` son:
1. [X] Identificar el riesgo que supone la manipulación de materiales, herramientas, útiles y máquinas.
2. [X] Identificar las causas más usuales de accidentes.
3. [X] Describir los elementos de seguridad.
4. [X] Relacionar la manipulación de materiales, herramientas, útiles y máquinas con las medidas de seguridad y protección requeridas.
5. [ ] Operar las máquinas respetando las normass de seguridad.
6. [X] Determinar las normas de seguridad y protección a adoptar en la ejecución de tareas.
7. [ ] Identificar las principales fuentes de contaminación en el entorno ambiental.
8. [ ] Clasificar los residuos generados para su retirada selectiva.
9. [X] Relacionar el orden y limpieza de instalaciones y equipos, con la prevención de riesgos.